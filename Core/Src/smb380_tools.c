/*
 * smb380_tools.c
 *
 *  Created on: 7 sep 2021
 *      Author: ssdmt@mail.ru, cyb.ssdmt@gmail.com
 */

#include "stm32f1xx_hal.h"
#include "smb380.h"

extern void _delay(uint32_t cnt); // main.c


// LOCAL PROPERTIES ---------------------------

static smb380_state_t _smb380;

static SPI_HandleTypeDef* _smb380_hspi;
static GPIO_TypeDef* _smb380_GPIOx;
static uint16_t _smb380_GPIO_Pin;

static uint8_t _spi_tx[2] = {0};


// INTERFACE ----------------------------------

int  smb380_init(smb380_spi_t *spi_info)
{
	if (BIT_TEST(_smb380.init, _STATUS_INIT_SMB380)) // all init
		return -1;

	_smb380_hspi = spi_info->hspi;
	_smb380_GPIOx = spi_info->GPIOx;
	_smb380_GPIO_Pin = spi_info->GPIO_Pin;

	BIT_SET(_smb380.init, _STATUS_INIT_SMB380);
	return 0;
}

void smb380_deinit()
{
	BIT_RESET(_smb380.init, _STATUS_INIT_SMB380);
	_smb380_hspi = 0;
	_smb380_GPIOx = 0;
	_smb380_GPIO_Pin = 0;
}

// return g-sensor data in 'spiRx' argument
int smb380_read(uint8_t addr, uint8_t *_spiRx, uint8_t num)
{
	if (!BIT_TEST(_smb380.init, _STATUS_INIT_SMB380)) // not init smb380
			return -1;
	if ((num > 6) || (num < 0)) return -2; // out of range Rx buffer length

	_spi_tx[0] = addr | 0x80;
	HAL_GPIO_WritePin(_smb380_GPIOx, _smb380_GPIO_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(_smb380_hspi, _spi_tx, 1, 500);
//	_delay(5);
	HAL_SPI_Receive(_smb380_hspi, _spiRx, num, 500);
	HAL_GPIO_WritePin(_smb380_GPIOx, _smb380_GPIO_Pin, GPIO_PIN_SET);
	return 0;
}

int smb380_write(uint8_t addr, uint8_t data) // write one byte only
{
	if (!BIT_TEST(_smb380.init, _STATUS_INIT_SMB380)) // not init smb380
			return -1;

	_spi_tx[0] = addr; _spi_tx[1] = data;
	HAL_GPIO_WritePin(_smb380_GPIOx, _smb380_GPIO_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(_smb380_hspi, _spi_tx, 2, 500);
	HAL_GPIO_WritePin(_smb380_GPIOx, _smb380_GPIO_Pin, GPIO_PIN_SET);
	return 0;
}

void smb380_accel(enum SMB380_e axis, uint16_t *accel)
{
	uint8_t spiRx[2];

	spiRx[0] = 0;
	while(!(spiRx[0] & 0x01)) // wait ready state for reading X,Y,Z accel data
		smb380_read(axis, spiRx, 1);
	smb380_read(axis, spiRx, 2);

	*accel = spiRx[1];
	*accel <<= 2;
	*accel |=  (spiRx[0] >> 6);
}

void smb380_conv2str(uint16_t hex, uint8_t *str)
{
	float val = 0;
	if (hex & 0x200) { // sign is '-'
		val = ((~hex & 0x01ff) + 1) * 0.004;
		snprintf(str, 10, "-%2.2f", val);
	} else { // sign is '+'
		val = hex * 0.004;
		snprintf(str, 10, "%2.2f", val);
	}
}
