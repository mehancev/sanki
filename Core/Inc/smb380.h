/*
 * smb380.h
 *
 *  Created on: 7 sep 2021
 *      Author: Author: ssdmt@mail.ru, cyb.ssdmt@gmail.com
 */

#ifndef INC_SMB380_H_
#define INC_SMB380_H_

#define BIT_SET(x, y)	((x) |= (y))
#define BIT_RESET(x, y)	((x) &= (~(y)))
#define BIT_TEST(x, y)	((x) & (y))

typedef struct smb380_spi_s {
	SPI_HandleTypeDef *hspi; // spi handler
	GPIO_TypeDef *GPIOx; // CS spi port
	uint16_t GPIO_Pin; // CS spi pin
} smb380_spi_t;

enum SMB380_e {
	SMB380_X_ACCEL = 0x02, // X: 02-03 addr
	SMB380_Y_ACCEL = 0x04, // Y: 04-05 addr
	SMB380_Z_ACCEL = 0x06, // Z: 06-07 addr
};


int  smb380_init(smb380_spi_t *spi_info);
void smb380_deinit();
int smb380_read(uint8_t addr, uint8_t *_spiRx, uint8_t num);
int smb380_write(uint8_t addr, uint8_t data);
void smb380_accel(enum SMB380_e axis, uint16_t *accel);
void smb380_conv2str(uint16_t hex, uint8_t *str);


typedef struct smb380_state_s {
	unsigned short init;
#define _STATUS_INIT_SMB380		(0x01)
	unsigned short reserved;
#define BIT_RESERVED_1			(0x01)
#define BIT_RESERVED_2			(0x02)
#define BIT_RESERVED_3			(0x04)
#define BIT_RESERVED_4			(0x08)
#define BIT_RESERVED_5			(0x10)
#define BIT_RESERVED_6			(0x20)
#define BIT_RESERVED_7			(0x40)
#define BIT_RESERVED_8			(0x80)

} smb380_state_t;




#endif /* INC_SMB380_H_ */
