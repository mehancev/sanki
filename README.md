
(*)
	For copy sanki.bin file to sanki_YMMDD_HHmm.bin file
	  run rename.py script in sanki folder.
	example:
	$ python3 rename.py <project_folder>
	$ python3 rename.py c:\work\sanki
	
(*) 
	https://ru.wikipedia.org/wiki/%D0%92%D1%81%D1%82%D0%B0%D0%B2%D0%BA%D0%B0_%D0%B1%D0%B0%D0%B9%D1%82%D0%BE%D0%B2_%D1%81_%D1%84%D0%B8%D0%BA%D1%81%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%BD%D0%BE%D0%B9_%D0%B8%D0%B7%D0%B1%D1%8B%D1%82%D0%BE%D1%87%D0%BD%D0%BE%D1%81%D1%82%D1%8C%D1%8E
	https://en.wikipedia.org/wiki/Consistent_Overhead_Byte_Stuffing